ideU the ide. Universal.

Multi OS, multi languages, multi compilers.

ideU is derived from MSEide by Martin Schreiber.
http://sourceforge.net/projects/mseide-msegui/

Sources are in /apps/ide/

There are binaries-executables in

=> https://github.com/fredvs/ideU

with plugin fpGUI uidesigner_ext included
for Windows, Linux 32 and 64 bit and freeBSD 64 bit.

Fred van Stappen
fiens@hotmail.com
