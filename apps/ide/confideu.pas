unit confideu;
{$ifdef FPC}{$mode objfpc}{$h+}{$endif}
interface
uses
 msetypes,mseglob,mseguiglob,mseguiintf,mseapplication,msegui,
 msegraphics,msegraphutils,mseclasses,mseforms,
 msegraphedits,msesimplewidgets;
type
 tconfideufo = class(tmseform)
   tbfilereload0: tbooleaneditradio;
   tbfilereload1: tbooleaneditradio;
   tbfilereload2: tbooleaneditradio;
   tlabel1: tlabel;
   ok: tbutton;
 end;
var
 confideufo: tconfideufo;
implementation
uses
 confideu_mfm;
end.
